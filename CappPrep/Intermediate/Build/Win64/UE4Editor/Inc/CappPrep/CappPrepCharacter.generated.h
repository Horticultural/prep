// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAPPPREP_CappPrepCharacter_generated_h
#error "CappPrepCharacter.generated.h already included, missing '#pragma once' in CappPrepCharacter.h"
#endif
#define CAPPPREP_CappPrepCharacter_generated_h

#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_RPC_WRAPPERS
#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACappPrepCharacter(); \
	friend struct Z_Construct_UClass_ACappPrepCharacter_Statics; \
public: \
	DECLARE_CLASS(ACappPrepCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(ACappPrepCharacter)


#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACappPrepCharacter(); \
	friend struct Z_Construct_UClass_ACappPrepCharacter_Statics; \
public: \
	DECLARE_CLASS(ACappPrepCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(ACappPrepCharacter)


#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACappPrepCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACappPrepCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACappPrepCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACappPrepCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACappPrepCharacter(ACappPrepCharacter&&); \
	NO_API ACappPrepCharacter(const ACappPrepCharacter&); \
public:


#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACappPrepCharacter(ACappPrepCharacter&&); \
	NO_API ACappPrepCharacter(const ACappPrepCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACappPrepCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACappPrepCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACappPrepCharacter)


#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ACappPrepCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ACappPrepCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ACappPrepCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ACappPrepCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ACappPrepCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ACappPrepCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ACappPrepCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ACappPrepCharacter, L_MotionController); }


#define CappPrep_Source_CappPrep_CappPrepCharacter_h_11_PROLOG
#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_RPC_WRAPPERS \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_INCLASS \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CappPrep_Source_CappPrep_CappPrepCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_INCLASS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_CappPrepCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CappPrep_Source_CappPrep_CappPrepCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
