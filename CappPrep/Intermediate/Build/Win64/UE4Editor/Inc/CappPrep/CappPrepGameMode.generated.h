// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAPPPREP_CappPrepGameMode_generated_h
#error "CappPrepGameMode.generated.h already included, missing '#pragma once' in CappPrepGameMode.h"
#endif
#define CAPPPREP_CappPrepGameMode_generated_h

#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_RPC_WRAPPERS
#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACappPrepGameMode(); \
	friend struct Z_Construct_UClass_ACappPrepGameMode_Statics; \
public: \
	DECLARE_CLASS(ACappPrepGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CappPrep"), CAPPPREP_API) \
	DECLARE_SERIALIZER(ACappPrepGameMode)


#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACappPrepGameMode(); \
	friend struct Z_Construct_UClass_ACappPrepGameMode_Statics; \
public: \
	DECLARE_CLASS(ACappPrepGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CappPrep"), CAPPPREP_API) \
	DECLARE_SERIALIZER(ACappPrepGameMode)


#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CAPPPREP_API ACappPrepGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACappPrepGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CAPPPREP_API, ACappPrepGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACappPrepGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CAPPPREP_API ACappPrepGameMode(ACappPrepGameMode&&); \
	CAPPPREP_API ACappPrepGameMode(const ACappPrepGameMode&); \
public:


#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CAPPPREP_API ACappPrepGameMode(ACappPrepGameMode&&); \
	CAPPPREP_API ACappPrepGameMode(const ACappPrepGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CAPPPREP_API, ACappPrepGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACappPrepGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACappPrepGameMode)


#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define CappPrep_Source_CappPrep_CappPrepGameMode_h_9_PROLOG
#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_RPC_WRAPPERS \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_INCLASS \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CappPrep_Source_CappPrep_CappPrepGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_INCLASS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_CappPrepGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CappPrep_Source_CappPrep_CappPrepGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
