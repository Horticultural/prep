// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAPPPREP_Interactable_SM_CompProfile_generated_h
#error "Interactable_SM_CompProfile.generated.h already included, missing '#pragma once' in Interactable_SM_CompProfile.h"
#endif
#define CAPPPREP_Interactable_SM_CompProfile_generated_h

#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_RPC_WRAPPERS
#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInteractable_SM_CompProfile(); \
	friend struct Z_Construct_UClass_UInteractable_SM_CompProfile_Statics; \
public: \
	DECLARE_CLASS(UInteractable_SM_CompProfile, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(UInteractable_SM_CompProfile)


#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUInteractable_SM_CompProfile(); \
	friend struct Z_Construct_UClass_UInteractable_SM_CompProfile_Statics; \
public: \
	DECLARE_CLASS(UInteractable_SM_CompProfile, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(UInteractable_SM_CompProfile)


#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInteractable_SM_CompProfile(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable_SM_CompProfile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInteractable_SM_CompProfile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable_SM_CompProfile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInteractable_SM_CompProfile(UInteractable_SM_CompProfile&&); \
	NO_API UInteractable_SM_CompProfile(const UInteractable_SM_CompProfile&); \
public:


#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInteractable_SM_CompProfile(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInteractable_SM_CompProfile(UInteractable_SM_CompProfile&&); \
	NO_API UInteractable_SM_CompProfile(const UInteractable_SM_CompProfile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInteractable_SM_CompProfile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable_SM_CompProfile); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable_SM_CompProfile)


#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_PRIVATE_PROPERTY_OFFSET
#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_13_PROLOG
#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_RPC_WRAPPERS \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_INCLASS \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_INCLASS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CappPrep_Source_CappPrep_Interactable_SM_CompProfile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
