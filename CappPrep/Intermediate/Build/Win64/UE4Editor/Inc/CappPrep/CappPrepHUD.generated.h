// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAPPPREP_CappPrepHUD_generated_h
#error "CappPrepHUD.generated.h already included, missing '#pragma once' in CappPrepHUD.h"
#endif
#define CAPPPREP_CappPrepHUD_generated_h

#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_RPC_WRAPPERS
#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACappPrepHUD(); \
	friend struct Z_Construct_UClass_ACappPrepHUD_Statics; \
public: \
	DECLARE_CLASS(ACappPrepHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(ACappPrepHUD)


#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACappPrepHUD(); \
	friend struct Z_Construct_UClass_ACappPrepHUD_Statics; \
public: \
	DECLARE_CLASS(ACappPrepHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(ACappPrepHUD)


#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACappPrepHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACappPrepHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACappPrepHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACappPrepHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACappPrepHUD(ACappPrepHUD&&); \
	NO_API ACappPrepHUD(const ACappPrepHUD&); \
public:


#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACappPrepHUD(ACappPrepHUD&&); \
	NO_API ACappPrepHUD(const ACappPrepHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACappPrepHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACappPrepHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACappPrepHUD)


#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define CappPrep_Source_CappPrep_CappPrepHUD_h_9_PROLOG
#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_RPC_WRAPPERS \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_INCLASS \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CappPrep_Source_CappPrep_CappPrepHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_INCLASS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_CappPrepHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CappPrep_Source_CappPrep_CappPrepHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
