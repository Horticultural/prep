// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAPPPREP_InteractableStaticMeshComponent_generated_h
#error "InteractableStaticMeshComponent.generated.h already included, missing '#pragma once' in InteractableStaticMeshComponent.h"
#endif
#define CAPPPREP_InteractableStaticMeshComponent_generated_h

#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_RPC_WRAPPERS
#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInteractableStaticMeshComponent(); \
	friend struct Z_Construct_UClass_UInteractableStaticMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UInteractableStaticMeshComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(UInteractableStaticMeshComponent)


#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUInteractableStaticMeshComponent(); \
	friend struct Z_Construct_UClass_UInteractableStaticMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UInteractableStaticMeshComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CappPrep"), NO_API) \
	DECLARE_SERIALIZER(UInteractableStaticMeshComponent)


#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInteractableStaticMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractableStaticMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInteractableStaticMeshComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractableStaticMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInteractableStaticMeshComponent(UInteractableStaticMeshComponent&&); \
	NO_API UInteractableStaticMeshComponent(const UInteractableStaticMeshComponent&); \
public:


#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInteractableStaticMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInteractableStaticMeshComponent(UInteractableStaticMeshComponent&&); \
	NO_API UInteractableStaticMeshComponent(const UInteractableStaticMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInteractableStaticMeshComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractableStaticMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractableStaticMeshComponent)


#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_12_PROLOG
#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_RPC_WRAPPERS \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_INCLASS \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_INCLASS_NO_PURE_DECLS \
	CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CappPrep_Source_CappPrep_InteractableStaticMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
