// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "InteractableStaticMeshComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class CAPPPREP_API UInteractableStaticMeshComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	
};
