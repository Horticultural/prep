// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CappPrepGameMode.h"
#include "CappPrepHUD.h"
#include "CappPrepCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACappPrepGameMode::ACappPrepGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ACappPrepHUD::StaticClass();
}
